package com.second.task;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MathTest {
	private Math math;

	@BeforeEach
	void setUp() {
		math = new Math();
	}

	@Test
	void add() {
		assertEquals(3, math.add(1, 2));
	}

	@Test
	void multiply() {
		assertEquals(6, math.multiply(2, 3));
	}

	@Test
	void multiplyByZero() {
		assertEquals(0, math.multiply(2, 0));
	}
}